var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var imageMin = require('gulp-imagemin');
var sass = require("gulp-sass");
var concat = require("gulp-concat");

var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var url = 'http://ventto.lob/';

function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

gulp.task('browser-sync', function() {
    //watch files
    var files = [
    './style.css',
    './*.php',
    './*.js',
    './*.html'
    ];
    browserSync.init(files, {
    proxy: {
      target: url,
    },
    // server: true,
    notify: false,
    injectChanges: true
    });
});

gulp.task('images', function() {
  gulp.src(['source/images/*.png','source/images/*.jpg'])
    .pipe(imageMin())
    .pipe(gulp.dest('assets/images/'))
    .pipe(browserSync.stream())
    .on("error", handleError);
});

gulp.task('sass', function () {
    gulp.src('source/sass/*.sass')
    .pipe(sass())
    .on("error", handleError)
    .pipe(gulp.dest('assets/css/'))
    .pipe(reload({stream:true}));
});

gulp.task('js', function() {
  gulp.src(['source/libs/dependencies/jquery.min.js','source/libs/dependencies/tether.min.js','source/libs/*.js','source/js/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('assets/js/'))
    .pipe(browserSync.stream())
    .on("error", handleError);
});

gulp.task('php', function() {
  gulp.src(['*.php'])
    .pipe(browserSync.stream())
    .on("error", handleError);
});

gulp.task('default', ['sass', 'browser-sync'], function(){
  gulp.watch('source/sass/*.sass', ['sass']);
  gulp.watch('source/images/*', ['images']);
  gulp.watch('source/js/*.js', ['js']);
  gulp.watch('/', ['php']);
});
